package com.henderson.finance.ui.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.core.view.GravityCompat
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import com.henderson.finance.R
import com.henderson.finance.ui.chart.ChartActivity
import com.henderson.finance.ui.dividend.DividendActivity

abstract class NavigationActivity<T : ViewDataBinding> : BaseActivity<T>(){
    abstract val drawer: DrawerLayout
    abstract val hamburger: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNavigation(drawer)
    }

    //Set event to Navigation
    private fun initNavigation(drawer: DrawerLayout) {
        hamburger.setOnClickListener { clickHamburger() }

        drawer.findViewById<View>(R.id.navigation_dividend).setOnClickListener {
            drawer.closeDrawer(GravityCompat.END)
            startActivity(Intent(this, DividendActivity::class.java))
        }
    }

    private fun clickHamburger() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END)
        } else {
            drawer.openDrawer(GravityCompat.END)
        }
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }
}
