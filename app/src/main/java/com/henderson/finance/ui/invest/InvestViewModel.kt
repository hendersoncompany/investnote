package com.henderson.finance.ui.invest

import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.henderson.finance.R
import com.henderson.finance.model.Investment
import com.henderson.finance.ui.base.BaseViewModel
import com.henderson.finance.utils.BaseUtil
import com.henderson.finance.utils.Constant.AVAILABLE_COUNT
import com.henderson.finance.utils.PreferenceManager
import com.henderson.finance.utils.db.DataBase

class InvestViewModel(application: Application) : BaseViewModel(application) {
    var listener: InvestListener? = null

    //Local Variable
    var availableCount = MutableLiveData<Int>()
    var investmentList = MutableLiveData<ArrayList<Investment>>()
    var insertingResult = MutableLiveData<Long>()

    //Local Variable For Total View
    var isTotalViewVisible = MutableLiveData(true)
    var totalPrice = MutableLiveData("")
    var totalCollection = MutableLiveData("")
    var totalEarningPrice = MutableLiveData("")
    var totalEarningRate = MutableLiveData("")

    fun clickArrow(view: View) {
        isTotalViewVisible.value = !isTotalViewVisible.value!!
    }

    fun initData() {
        availableCount.value = PreferenceManager.getInt(getApplication(), AVAILABLE_COUNT)
        getInvestmentList()
    }

    fun setTotalData() {
        if (investmentList.value == null) {
            return
        }
        var price = 0
        var collection = 0
        for (investment in investmentList.value!!) {
            price += investment.price
            collection += investment.collection
        }
        var earningPrice = collection - price
        var earningRate = if (price == 0 || earningPrice == 0) { 0f } else { (earningPrice / price.toFloat()) * 100 }

        totalPrice.value = BaseUtil.addComma(price)
        totalCollection.value = BaseUtil.addComma(collection)
        totalEarningPrice.value = (if (earningPrice > 0) "+" else "") + BaseUtil.addComma(earningPrice)
        totalEarningRate.value = (if (earningRate > 0) "+" else "") + getApplication<Application>().resources.getString(R.string.percent, earningRate)
    }

    fun getInvestmentList() {
        Thread {
            investmentList.postValue(DataBase.getInstance(getApplication())?.investmentDao()?.getInvestmentList() as ArrayList<Investment>)
        }.start()
    }

    fun addInvestment(investment: Investment) {
        Thread {
            val result = DataBase.getInstance(getApplication())?.investmentDao()?.insertInvestment(investment)
            insertingResult.postValue(result)
            PreferenceManager(getApplication()).removeAvailableCount()
            availableCount.postValue(PreferenceManager.getInt(getApplication(), AVAILABLE_COUNT))
        }.start()
    }

    fun showAddInvestmentDialog(view: View) {
        val coin = PreferenceManager.getInt(getApplication(), AVAILABLE_COUNT)
        if (coin > 0) {
            listener?.showAddInvestmentDialog()
        } else {
            toast(getApplication(), R.string.toast_no_coin)
        }
    }

    fun showRewardedVideoAd(view: View) {
        listener?.showRewardedVideoAd()
    }
}

interface InvestListener {
    fun showRewardedVideoAd()
    fun showAddInvestmentDialog()
}
