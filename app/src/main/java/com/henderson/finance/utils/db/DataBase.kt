package com.henderson.finance.utils.db

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.henderson.finance.model.Dividend
import com.henderson.finance.model.Investment

@androidx.room.Database(entities = [Investment::class, Dividend::class], version = 3, exportSchema = false)
abstract class DataBase : RoomDatabase() {
    abstract fun investmentDao(): InvestmentDao
    abstract fun dividendDao(): DividendDao

    //싱글톤 디자인 패턴 적용
    companion object {
        private var INSTANCE: DataBase? = null

        private val migration_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE tb_investment RENAME TO temp_investment")
                database.execSQL("CREATE TABLE tb_investment (" +
                        "id INTEGER NOT NULL PRIMARY KEY," +
                        "category TEXT," +
                        "price INTEGER NOT NULL," +
                        "collection INTEGER NOT NULL," +
                        "fromDate TEXT," +
                        "toDate TEXT)")

                database.execSQL("INSERT INTO tb_investment(id, category, price, collection, fromDate, toDate) SELECT * FROM temp_investment")
                database.execSQL("DROP TABLE temp_investment")
            }
        }

        private val migration_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE tb_dividend (" +
                        "id INTEGER NOT NULL PRIMARY KEY," +
                        "category TEXT," +
                        "dividend INTEGER NOT NULL," +
                        "date TEXT)")
            }
        }

        fun getInstance(context: Context): DataBase? {
            if (INSTANCE == null) {
                synchronized(DataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, DataBase::class.java, "Investment.db")
                            .addMigrations(migration_1_2, migration_2_3)
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
